package com.traveler.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class dbAccess {

    public static Connection getConnection() {


        Connection connection = null;
        try {
            //192.168.176.128
            Class.forName("org.gjt.mm.mysql.Driver");
            String url = "jdbc:mysql://localhost/traveler";
            connection = DriverManager.getConnection(url, "root",
                    "root");

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
        }
        return connection;
    }

    public static void main(String args[]) {

        long startTime = System.currentTimeMillis();
        Connection connection = dbAccess.getConnection();
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println(totalTime);

    }
}

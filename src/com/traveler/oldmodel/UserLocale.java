package com.traveler.oldmodel;

public class UserLocale {

    private String country;
    private String code;

    public UserLocale(String country, String code) {
        super();
        this.country = country;
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}

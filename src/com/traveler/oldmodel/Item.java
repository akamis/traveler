package com.traveler.oldmodel;

import java.util.ArrayList;
import java.util.Map;

public class Item {

    private int id;
    private String name;
    private int typeId;
    private String type;
    private String destinationName;
    private Destination destination;
    private Coordinate coordinate;
    private Map<String, Object> description;
    private ArrayList<Map<String, Object>> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        switch (typeId) {
            case 1:
                type = "Destination";
                break;
            case 2:

                type = "Hotel";

                break;
        }
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Map<String, Object> getDescription() {
        return description;
    }

    public void setDescription(Map<String, Object> description) {
        this.description = description;
    }

    public String search() {

        return getType();
    }

    public ArrayList<Map<String, Object>> getImages() {
        return images;
    }

    public void setImages(ArrayList<Map<String, Object>> images) {
        this.images = images;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String toString() {
        return "id :" + id + "\tname:\t" + name + "\n" + "\ttype:\t" + typeId
                + "\n";
    }
}
package com.traveler.oldmodel;

public class Destination extends Item {

    private int hotelCount;

    public int getHotelCount() {
        return hotelCount;
    }

    public void setHotelCount(int hotelCount) {
        this.hotelCount = hotelCount;
    }

}

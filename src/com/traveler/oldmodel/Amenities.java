package com.traveler.oldmodel;

/**
 * Created by nexaas on 02/04/16.
 */
public class Amenities {

    private int amenityID;
    private String name;
    private int type;

    public int getAmenityID() {
        return amenityID;
    }

    public void setAmenityID(int amenityID) {
        this.amenityID = amenityID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

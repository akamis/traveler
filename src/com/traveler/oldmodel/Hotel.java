package com.traveler.oldmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Hotel extends Location implements Serializable {

    private int star;
    private int roomCount;
    private List<Review> reviews;
    private List<Amenity> amenities;
    private List<Affilate> affiliates;

    public int getStar() {
        return star;
    }

    public List<Affilate> getAffiliates() {
        return affiliates;
    }

    public void setAffiliates(ArrayList<Affilate> affiliates) {
        this.affiliates = affiliates;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }


    public List<Amenity> getAmenities() {
        return amenities;
    }

    public void setAmenities(ArrayList<Amenity> amenities) {
        this.amenities = amenities;
    }

    public String getAffiliateIdList() {
        System.out.println("getAffiliateIdList");
        String urls = "";
        if (affiliates != null) {
            for (int i = 0; i < affiliates.size(); i++) {
                if (affiliates.get(i).getIsChecked()) {
                    urls += affiliates.get(i).getHost() + "/" + affiliates.get(i).getUri();
                    if (i != affiliates.size() - 1) urls += ",";
                } else {

                    System.out.println(affiliates.get(i).getIsChecked());
                }
            }
        }
        return urls;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

}
package com.traveler.oldmodel;

import java.sql.Date;

import org.brickred.socialauth.Profile;

public class Review {

    private long reviewID;
    private Profile author;
    private Hotel hotel;
    private Integer rating;
    private String title;
    private String review;
    private String tripType;
    private Date tripDate;
    private boolean isBoutiqueHotel;
    private boolean isRomanticHotel;
    private boolean isCharmingotel;
    private Integer serviceRating;
    private Integer sleepQualityRating;
    private Integer locationRating;
    private Integer swimmingPoolRating;
    private Integer valueRating;
    private Integer cleanlinessRating;
    private Integer roomsRating;
    private Integer fitnessFacilityRating;
    private String roomTip;

    public long getReviewID() {
        return reviewID;
    }

    public void setReviewID(long reviewID) {
        this.reviewID = reviewID;
    }


    public Profile getAuthor() {
        return author;
    }

    public void setAuthor(Profile author) {
        this.author = author;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public Date getTripDate() {
        return tripDate;
    }

    public void setTripDate(Date tripDate) {
        this.tripDate = tripDate;
    }

    public boolean isBoutiqueHotel() {
        return isBoutiqueHotel;
    }

    public void setBoutiqueHotel(boolean isBoutiqueHotel) {
        this.isBoutiqueHotel = isBoutiqueHotel;
    }

    public boolean isRomanticHotel() {
        return isRomanticHotel;
    }

    public void setRomanticHotel(boolean isRomanticHotel) {
        this.isRomanticHotel = isRomanticHotel;
    }

    public boolean isCharmingotel() {
        return isCharmingotel;
    }

    public void setCharmingotel(boolean isCharmingotel) {
        this.isCharmingotel = isCharmingotel;
    }

    public Integer getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(Integer serviceRating) {
        this.serviceRating = serviceRating;
    }

    public Integer getSleepQualityRating() {
        return sleepQualityRating;
    }

    public void setSleepQualityRating(Integer sleepQualityRating) {
        this.sleepQualityRating = sleepQualityRating;
    }

    public Integer getLocationRating() {
        return locationRating;
    }

    public void setLocationRating(Integer locationRating) {
        this.locationRating = locationRating;
    }

    public Integer getSwimmingPoolRating() {
        return swimmingPoolRating;
    }

    public void setSwimmingPoolRating(Integer swimmingPoolRating) {
        this.swimmingPoolRating = swimmingPoolRating;
    }

    public Integer getValueRating() {
        return valueRating;
    }

    public void setValueRating(Integer valueRating) {
        this.valueRating = valueRating;
    }

    public Integer getCleanlinessRating() {
        return cleanlinessRating;
    }

    public void setCleanlinessRating(Integer cleanlinessRating) {
        this.cleanlinessRating = cleanlinessRating;
    }

    public Integer getRoomsRating() {
        return roomsRating;
    }

    public void setRoomsRating(Integer roomsRating) {
        this.roomsRating = roomsRating;
    }

    public Integer getFitnessFacilityRating() {
        return fitnessFacilityRating;
    }

    public void setFitnessFacilityRating(Integer fitnessFacilityRating) {
        this.fitnessFacilityRating = fitnessFacilityRating;
    }

    public String getRoomTip() {
        return roomTip;
    }

    public void setRoomTip(String roomTip) {
        this.roomTip = roomTip;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return "rating: " + rating + "\ntitle : " + title + "\ntrip type: "
                + tripType;
    }
}

package com.traveler.oldmodel;

import java.io.Serializable;

public class Amenity implements Serializable {

    /**
     *
     */
    private int id;
    private String name;
    private int hotelCount;
    private boolean checked;

    public Amenity(int id, String name, int hotelCount) {
        this.id = id;
        this.name = name;
        this.hotelCount = hotelCount;
    }

    public Amenity(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHotelCount() {
        return hotelCount;
    }

    public void setHotelCount(int hotelCount) {
        this.hotelCount = hotelCount;
    }

    @Override
    public String toString() {
        return id + "\t" + name + "\t" + hotelCount + "\n";
    }

}

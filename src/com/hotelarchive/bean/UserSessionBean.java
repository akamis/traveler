package com.hotelarchive.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;

import com.hotelarchive.business.ProfileBusiness;

@ManagedBean
@SessionScoped
public class UserSessionBean implements Serializable {

    private SocialAuthManager manager;
    private String currentlURL = "http://localhost:8080/Traveler/";
    private String providerID;
    private Profile profile;

    private String firstName;
    private String lastName;
    private String city;

    private String email;
    private String password;
    ProfileBusiness profileBusiness = new ProfileBusiness();

    public UserSessionBean() {
    }

    public String register() {
        if (profileBusiness.registerUser(firstName, lastName, email,
                password, city)) {

            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Congratulations!",
                    "You have successfully registered with traveler.com ");
            context.addMessage(null, message);
            return "success";
        } else {
            return "fail";
        }

    }

    public String logIn() {

        profile = profileBusiness.loginUser(email, password);

        if (profile != null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext()
                        .redirect(currentlURL);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Login Failure", "Invalid Credidentials");
        context.addMessage(null, message);
        return "";

    }

    public String socialConnect() throws Exception {
        Properties props = System.getProperties();
        props.put("graph.facebook.com.consumer_key", "306652303103482");
        props.put("graph.facebook.com.consumer_secret",
                "d8c9a23b9cafeee1e3a5be7059bff540");
        props.put("graph.facebook.com.custom_permissions",
                "email,user_birthday,user_location");

        SocialAuthConfig config = SocialAuthConfig.getDefault();
        config.load(props);
        manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);

        ExternalContext externalContext = FacesContext.getCurrentInstance()
                .getExternalContext();
        String successURL = "http://localhost:8080/Traveler/SocialLoginSuccess.jsf";

        String authenticationURL = manager.getAuthenticationUrl(providerID,
                successURL);
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(authenticationURL);
        return "";
    }

    public String pullUserInfo() {
        try {
            // Pull user's data from the provider
            ExternalContext externalContext = FacesContext.getCurrentInstance()
                    .getExternalContext();
            HttpServletRequest request = (HttpServletRequest) externalContext
                    .getRequest();
            Map map = SocialAuthUtil.getRequestParametersMap(request);
            if (this.manager != null) {
                AuthProvider provider = manager.connect(map);
                this.profile = provider.getUserProfile();
            }
            FacesContext.getCurrentInstance().getExternalContext()
                    .redirect(currentlURL);
        } catch (Exception ex) {
            System.out.println("UserSession - Exception: " + ex.toString());
        }
        return "";
    }

    public String logOut() {

        if (providerID != null)
            manager.disconnectProvider(providerID);
        profile = null;
        FacesContext ctx = FacesContext.getCurrentInstance();
        String path = ctx.getExternalContext().getRequestContextPath();

        try {
            FacesContext.getCurrentInstance().getExternalContext()
                    .redirect(currentlURL);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    public String getCurrentlURL() {
        return currentlURL;
    }

    public void setCurrentlURL() {
        currentlURL = ((HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest()).getRequestURL().toString();
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    public Profile getProfile() {
        return profile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
package com.hotelarchive.bean;

import com.hotelarchive.message.*;
import com.traveler.oldmodel.*;
import com.hotelarchive.business.*;

import java.io.IOException;
import java.io.Serializable;

import java.util.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class HotelsBean implements Serializable {

    public static final String HOTEL_NAME_FIELD = "name";
    public static final String SORT_AS_ASCENDING = "asc";
    HotelSearchRequest hotelSearchRequest;
    private static final long serialVersionUID = 1L;
    protected int destinationId;
    protected Destination destination;
    private String destinationImageUrl;
    private String view = "block";
    private int hotelCount;
    private List<com.hotelarchive.model.Hotel> hotelList;
    private List<com.traveler.oldmodel.Hotel> hotels;
    private ArrayList<Map<String, Object>> destinationList;
    protected static DestinationBusiness destinationBusiness = new DestinationBusiness();
    protected static AmenityBusiness amenityBusiness = new AmenityBusiness();
    private List<Map<String, Object>> pages;

    public void prevPage() {
        int pageNo = hotelSearchRequest.getPageNo();
        if (pageNo == 1) {
            pageNo = (int) Math.ceil((double) hotelCount / 6);
        } else {
            pageNo--;
        }
        hotelSearchRequest.setPageNo(pageNo);

        updateHotels();
    }

    public void nextPage() {
        int pageNo = hotelSearchRequest.getPageNo();
        if (pageNo == (int) Math.ceil((double) hotelCount / 6)) {
            pageNo = 1;
        } else {
            pageNo++;
        }
        hotelSearchRequest.setPageNo(pageNo);
        updateHotels();
    }

    public HotelsBean() {
        hotelSearchRequest = new HotelSearchRequest();
        hotelSearchRequest.setOrderBy(HOTEL_NAME_FIELD);
        hotelSearchRequest.setOrderType(SORT_AS_ASCENDING);
        hotelSearchRequest.setPageNo(0);
        hotelSearchRequest.setAmenityList(new ArrayList<Amenity>());
        hotelSearchRequest.setFilterStarList(new ArrayList<Amenity>());
        hotelSearchRequest.setLimit(6);
    }

    public List<Map<String, Object>> getPages() {
        return pages;
    }

    public int getDestinationId() {
        return (destinationId);
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
//		destination = destinationBusiness.findDestination(destinationId);

    }


    public void goPage(String pNo) {
        int pageNo = Integer.parseInt(pNo);
        hotelSearchRequest.setPageNo(pageNo);
        updateHotels();
    }

    public int getHotelCount() {
        return hotelCount;
    }

    public List<com.traveler.oldmodel.Hotel> getHotels() {
        return (hotels);
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public ArrayList<Map<String, Object>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(
            ArrayList<Map<String, Object>> destinationList) {
        this.destinationList = destinationList;
    }


    public Destination getDestination() {
        return destination;
    }


    public List<com.hotelarchive.model.Hotel> getHotelList() {
        return hotelList;
    }

    public void setHotelList(List<com.hotelarchive.model.Hotel> hotelList) {
        this.hotelList = hotelList;
    }

    public String searchHotels() {
        HotelBusiness hotelBusiness = new HotelBusiness();
        HotelSearchResponse hotelSearchResponse;
        hotelSearchResponse = hotelBusiness.listHotels(hotelSearchRequest);

        hotelList = hotelSearchResponse.getHotelList();
        hotelCount = hotelSearchResponse.getHotelCount();

//		destinationList = destinationBusiness
//				.getDestinationMap(destinationId);
//
//		amenityList = amenityBusiness.getAmenities(destinationId);
//
//		filterStarList = amenityBusiness.getFilterStars(destinationId);
//		paging();
//
//        try {
//            FacesContext.getCurrentInstance().getExternalContext()
//                    .redirect("Hotels.jsf");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        return "Hotels";
    }

    public void updateHotels() {
        HotelBusiness hotelBusiness = new HotelBusiness();
        HotelSearchResponse hotelSearchResponse;
        hotelSearchResponse = hotelBusiness.listHotels(hotelSearchRequest);

        hotelList = hotelSearchResponse.getHotelList();
        hotelCount = hotelSearchResponse.getHotelCount();

        paging();
    }


    public List<Integer> paging() {
        int pageNo = hotelSearchRequest.getPageNo();

        List<Integer> pageList = new ArrayList<Integer>();
        int pcount = (int) Math.ceil((double) hotelCount / 6);
        int plimit = (pcount < 16) ? pcount : 16;

        int prev = pageNo;
        int next = pageNo;
        pageList.add(pageNo);
        do {
            if (prev - 1 > 0) {
                prev--;
                pageList.add(prev);
                plimit--;
            }
            if (next + 1 < pcount) {
                next++;
                pageList.add(next);
                plimit--;
            }
        } while (plimit > 0 && ((prev - 1) > 0 || (next + 1) < pcount));

        if (!pageList.contains(1)) {
            pageList.add(1);
        }
        if (!pageList.contains(pcount)) {
            pageList.add(pcount);
        }
        Collections.sort(pageList);
        pages = new ArrayList<Map<String, Object>>();
        Map<String, Object> page;
        for (int p = 0; p < pageList.size(); p++) {
            if (p == 1 && pageList.get(p) != 2) {
                page = new HashMap<String, Object>();
                page.put("pageNo", "...");
                page.put("class", "dots");
                pages.add(page);

            }
            page = new HashMap<String, Object>();
            page.put("pageNo", pageList.get(p));
            if (pageList.get(p) == pageNo) {
                page.put("class", "active");
            } else {

                page.put("class", "");
            }
            pages.add(page);

            if (p == pageList.size() - 2 && pageList.get(p) != pcount - 1) {
                page = new HashMap<String, Object>();
                page.put("pageNo", "...");
                page.put("class", "dots");
                pages.add(page);

            }
        }
        return pageList;
    }

    public static Object[] createArray(int size) {
        return new Object[size];
    }

    public String getImageThumbUrl(com.hotelarchive.model.Hotel hotel) {
        String url = "#";
        if (hotel.getImages().size() > 0) {
            String imageID = hotel.getImages().get(0).getId() + "";
            url = "//imgec.trivago.com/" + hotel.getImages().get(0).getDirectory() + "/" + imageID.substring(0, 2) + "/" + imageID.substring(2, 4) + "/" + imageID + "_mx.jpeg";

        }

        return url;
    }

    public HotelSearchRequest getHotelSearchRequest() {
        return hotelSearchRequest;
    }

    public void setHotelSearchRequest(HotelSearchRequest hotelSearchRequest) {
        this.hotelSearchRequest = hotelSearchRequest;
    }
}
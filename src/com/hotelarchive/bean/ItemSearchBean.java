package com.hotelarchive.bean;

import com.hotelarchive.message.ItemSearchResponse;
import com.hotelarchive.model.Item;
import com.hotelarchive.util.HibernateUtil;
import com.hotelarchive.business.HotelBusiness;
import com.hotelarchive.business.ItemBusiness;
import org.primefaces.context.RequestContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@ManagedBean
@SessionScoped
public class ItemSearchBean {

    List<Item> items;
    Item item;
    String key;
    Date datein = new Date();
    Date dateout = new Date();
    boolean display = false;
    private ItemBusiness itemBusiness = new ItemBusiness();
    protected static HotelBusiness hotelBusiness = new HotelBusiness();

    public ItemSearchBean() {

    }

    public void searchItem() {
        ItemBusiness itemBusiness = new ItemBusiness();
        ItemSearchResponse itemSearchResponse = itemBusiness.listItems(key);
        items = itemSearchResponse.getItems();
        display = (items.size() > 0) ? true : false;

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean getDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;

    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        setDisplay(false);
        key = item.getItemName();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("initCalendar()");
        this.item = item;
    }

    public String submit() {
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext
                    .getCurrentInstance().getExternalContext().getResponse();
            FacesContext.getCurrentInstance().responseComplete();


            if (item.getItemType() == 1) {
                response.sendRedirect("SearchHotels.jsf?view=block&destinationId=" + item.getItemId());

            } else if (item.getItemType() == 2) {
                response.sendRedirect("HotelReview.jsf?hotelId=" + item.getItemId());


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public Date getDatein() {
        return datein;
    }

    public void setDatein(Date datein) {
        this.datein = datein;
    }

    public Date getDateout() {
        return dateout;
    }

    public void setDateout(Date dateout) {
        this.dateout = dateout;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}


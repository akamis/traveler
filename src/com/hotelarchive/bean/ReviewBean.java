package com.hotelarchive.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.traveler.oldmodel.Review;
import com.hotelarchive.business.ReviewBusiness;

@ManagedBean
@ViewScoped
public class ReviewBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Review review;
    private HotelReviewBean hotelReviewBean;
    private UserSessionBean userSessionBean;
    private ReviewBusiness reviewBusiness = new ReviewBusiness();

    public ReviewBean() {
        hotelReviewBean = (HotelReviewBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("hotelReviewView");
        userSessionBean = (UserSessionBean) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("userSession");

        review = new Review();
        review.setAuthor(userSessionBean.getProfile());
//		review.setHotel(hotelReviewBean.getHotel());
    }

    public String saveReview() {
        if (reviewBusiness.addReview(review)) {
            return "success";
        } else {
            return "fail";
        }
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

}

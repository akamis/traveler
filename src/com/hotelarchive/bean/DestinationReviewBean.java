package com.hotelarchive.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.traveler.oldmodel.*;
import com.hotelarchive.business.*;

@ManagedBean
@SessionScoped
public class DestinationReviewBean {

    protected int destinationId;
    protected int pageNo = 1;
    private int destinationCount;
    protected Destination destination;
    private List<Destination> destinations;
    private ArrayList<Map<String, Object>> destinationList;
    protected static DestinationBusiness destinationBusiness = new DestinationBusiness();

    public int getDestinationId() {
        return (destinationId);
    }

    public void setDestinationId(int destinationId) {

        this.destinationId = destinationId;
        destination = destinationBusiness.findDestination(destinationId);
        destinationList = destinationBusiness
                .getDestinationMap(destinationId);
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getDestinationCount() {
        return destinationCount;
    }

    public Destination getDestination() {
        return (destination);
    }

    public List<Destination> getDestinations() {
        return (destinations);
    }

    public ArrayList<Map<String, Object>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(
            ArrayList<Map<String, Object>> destinationList) {
        this.destinationList = destinationList;
    }

    public String hotelsPage() {
        ExternalContext context = FacesContext.getCurrentInstance()
                .getExternalContext();
        try {
            context.redirect("Hotels.jsf?view=list&destinationId="
                    + destinationId);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";

    }

    public static void main(String args[]) {
        DestinationReviewBean d = new DestinationReviewBean();
        d.setDestinationId(90473);

    }
}
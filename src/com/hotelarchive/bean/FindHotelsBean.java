package com.hotelarchive.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.traveler.oldmodel.Hotel;
import com.hotelarchive.business.HotelBusiness;

@ManagedBean
@ApplicationScoped
public class FindHotelsBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<Hotel> topDealsHotels;

    protected static HotelBusiness hotelBusiness = new HotelBusiness();

    public List<Hotel> getTopDealsHotels() {
        return topDealsHotels;
    }

    public void setTopDealsHotels(List<Hotel> topDealsHotels) {
        this.topDealsHotels = topDealsHotels;
    }


    public void updateTopDealsHotels() {
        topDealsHotels = hotelBusiness.findPopularHotels(11, 9);
    }

    @PostConstruct
    public void init() {
        updateTopDealsHotels();

    }
}

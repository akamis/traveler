package com.hotelarchive.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.traveler.oldmodel.Hotel;
import com.hotelarchive.business.HotelBusiness;

@ManagedBean
@ApplicationScoped
public class HomeBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<Hotel> hotDealHotels;
    private List<Hotel> trendyHotels;
    private List<Hotel> specialHotels;

    protected static HotelBusiness hotelBusiness = new HotelBusiness();

    public List<Hotel> getHotDealHotels() {
        return hotDealHotels;
    }

    public void setHotDealHotels(List<Hotel> hotDealHotels) {
        this.hotDealHotels = hotDealHotels;
    }

    public List<Hotel> getTrendyHotels() {
        return trendyHotels;
    }

    public void setTrendyHotels(List<Hotel> trendyHotels) {
        this.trendyHotels = trendyHotels;
    }

    public List<Hotel> getSpecialHotels() {
        return specialHotels;
    }

    public void setSpecialHotels(List<Hotel> specialHotels) {
        this.specialHotels = specialHotels;
    }

    public void updateHotDealHotels() {
        hotDealHotels = hotelBusiness.findPopularHotels(14, 4);
    }

    public void updateTrendyHotels() {
        trendyHotels = hotelBusiness.findPopularHotels(12, 4);
    }

    public void updateSpecialHotels() {
        specialHotels = hotelBusiness.findPopularHotels(31, 4);
    }

    @PostConstruct
    public void init() {
//		updateHotDealHotels();
//		updateTrendyHotels();
//		updateSpecialHotels();
    }

}

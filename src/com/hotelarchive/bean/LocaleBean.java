package com.hotelarchive.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LocaleBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String locale;

    @PostConstruct
    public void init() {
        this.locale = "tr";
    }

    private static Map<String, Object> locales;

    static {
        locales = new LinkedHashMap<String, Object>();
        locales.put("Canada", new Locale("ca"));
        locales.put("Brasil", new Locale("br"));
        locales.put("Italia", new Locale("it"));
        locales.put("Deutschland", new Locale("de"));
        locales.put("France", new Locale("fr"));
        locales.put("Sverige", new Locale("se"));
        locales.put("Turkey", new Locale("tr"));
        locales.put("United Kingdom", new Locale("uk"));

    }

    public Map<String, Object> getLocales() {
        return locales;
    }

    public static void setLocales(Map<String, Object> locales) {
        LocaleBean.locales = locales;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
        FacesContext.getCurrentInstance().getViewRoot()
                .setLocale(new Locale(locale));
    }

    public List<String> getLocaleList() {
        List<String> l = new ArrayList<String>();

        for (Map.Entry<String, Object> entry : ((Map<String, Object>) locales)
                .entrySet()) {
            l.add(entry.getValue().toString());
        }
        return l;
    }


}
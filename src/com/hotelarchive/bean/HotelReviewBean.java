package com.hotelarchive.bean;

import com.hotelarchive.message.HotelDetailResponse;
import com.hotelarchive.model.*;
import com.traveler.oldmodel.*;
import com.hotelarchive.business.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.traveler.oldmodel.Hotel;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

@ManagedBean
@SessionScoped
public class HotelReviewBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int hotelId;
    private int destinationId;
    private int hotelCount;
    private com.hotelarchive.model.Hotel hotel;
    private HotelDetailResponse hotelDetailResponse;
    private MapModel mapModel;
    private ArrayList<Map<String, Object>> destinationList;

    private static HotelBusiness hotelBusiness = new HotelBusiness();
    private static DestinationBusiness destinationBusiness = new DestinationBusiness();

    private List<Hotel> similarHotels;

    private Integer averageRating;
    private Integer averageServiceRating;
    private Integer averageSleepQualityRating;
    private Integer averageLocationRating;
    private Integer averageSwimmingPoolRating;
    private Integer averageValueRating;
    private Integer averageCleanlinessRating;
    private Integer averageRoomsRating;
    private Integer averageFitnessFacilityRating;

    public int getHotelId() {
        return (hotelId);
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;

        hotelDetailResponse = hotelBusiness.getHotelDetail(hotelId);
        hotel = hotelDetailResponse.getHotel();
//        destinationList = destinationBusiness.getDestinationMap(hotel.getDestinationID());
//        destinationId = hotel.getDestinationID();
//        int k = 0;
//        averageRating = 0;
//        averageServiceRating = 0;
//        averageSleepQualityRating = 0;
//        averageLocationRating = 0;
//        averageSwimmingPoolRating = 0;
//        averageValueRating = 0;
//        averageCleanlinessRating = 0;
//        averageRoomsRating = 0;
//        averageFitnessFacilityRating = 0;
//        for (Review review : hotel.getReviews()) {
//            averageRating += review.getRating();
//            averageServiceRating += review.getServiceRating();
//            averageSleepQualityRating += review.getSleepQualityRating();
//            averageLocationRating += review.getLocationRating();
//            averageSwimmingPoolRating += review.getSwimmingPoolRating();
//            averageValueRating += review.getValueRating();
//            averageCleanlinessRating += review.getCleanlinessRating();
//            averageRoomsRating += review.getRoomsRating();
//            averageFitnessFacilityRating += review.getFitnessFacilityRating();
//            k++;
//
//        }
//        if (k != 0) {
//            averageRating /= k;
//            averageServiceRating /= k;
//            averageSleepQualityRating /= k;
//            averageLocationRating /= k;
//            averageSwimmingPoolRating /= k;
//            averageValueRating /= k;
//            averageCleanlinessRating /= k;
//            averageRoomsRating /= k;
//            averageFitnessFacilityRating /= k;
//        }
//        updateSimilarHotels();

        mapModel = new DefaultMapModel();

        LatLng coord = new LatLng(hotel.getLatitude(), hotel.getLongitude());
        mapModel.addOverlay(new Marker(coord, hotel.getName()));

    }

    public int getDestinationId() {
        return (destinationId);
    }

    public int getHotelCount() {
        return hotelCount;
    }


    public ArrayList<Map<String, Object>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(
            ArrayList<Map<String, Object>> destinationList) {
        this.destinationList = destinationList;
    }

    public List<Hotel> getSimilarHotels() {
        return similarHotels;
    }

    public void updateSimilarHotels() {
        similarHotels = hotelBusiness.findPopularHotels(destinationId, 5);
    }

    public String getHotelsPage() {

        return "Hotels.jsf?view=list&destinationId=" + hotel.getDestinationId();

    }

    public Integer getAverageServiceRating() {
        return averageServiceRating;
    }

    public Integer getAverageSleepQualityRating() {
        return averageSleepQualityRating;
    }

    public Integer getAverageLocationRating() {
        return averageLocationRating;
    }

    public Integer getAverageSwimmingPoolRating() {
        return averageSwimmingPoolRating;
    }

    public Integer getAverageValueRating() {
        return averageValueRating;
    }

    public Integer getAverageCleanlinessRating() {
        return averageCleanlinessRating;
    }

    public Integer getAverageRoomsRating() {
        return averageRoomsRating;
    }

    public Integer getAverageFitnessFacilityRating() {
        return averageFitnessFacilityRating;
    }

    public Integer getAverageRating() {
        return averageRating;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public com.hotelarchive.model.Hotel getHotel() {
        return hotel;
    }

    public String getImageUrl(com.hotelarchive.model.Image image) {
        String url = "#";
        String imageID = image.getId() + "";
        url = "//imgec.trivago.com/" + image.getDirectory() + "/" + imageID.substring(0, 2) + "/" + imageID.substring(2, 4) + "/" + imageID + "_l.jpeg";

        return url;
    }
}
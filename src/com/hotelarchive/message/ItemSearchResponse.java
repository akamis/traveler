package com.hotelarchive.message;


import com.hotelarchive.model.Hotel;
import com.hotelarchive.model.Item;

import java.util.List;

public class ItemSearchResponse {

    List<Item> items;

    public ItemSearchResponse(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}

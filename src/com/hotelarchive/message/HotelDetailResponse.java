package com.hotelarchive.message;


import com.hotelarchive.model.Hotel;

import java.util.List;

public class HotelDetailResponse {

    Hotel hotel;

    public HotelDetailResponse(Hotel hotel) {
        this.hotel = hotel;
    }

    public Hotel getHotel() {
        return hotel;
    }
}

package com.hotelarchive.message;


import com.hotelarchive.model.Hotel;
import com.traveler.oldmodel.Amenity;

import java.util.List;

public class HotelSearchRequest {

    int destinationId;
    int pageNo;
    int limit;
    String orderBy;
    String orderType;
    List<Amenity> amenityList;
    List<Amenity> filterStarList;


    public HotelSearchRequest() {
    }


    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public List<Amenity> getAmenityList() {
        return amenityList;
    }

    public void setAmenityList(List<Amenity> amenityList) {
        this.amenityList = amenityList;
    }

    public List<Amenity> getFilterStarList() {
        return filterStarList;
    }

    public void setFilterStarList(List<Amenity> filterStarList) {
        this.filterStarList = filterStarList;
    }
}

package com.hotelarchive.message;


import com.hotelarchive.model.Hotel;

import java.util.List;

public class HotelSearchResponse {

    List<Hotel> hotelList;
    int hotelCount;

    public HotelSearchResponse(List<Hotel> hotelList, int hotelCount) {
        this.hotelList = hotelList;
        this.hotelCount = hotelCount;
    }

    public List<Hotel> getHotelList() {
        return hotelList;
    }

    public int getHotelCount() {
        return hotelCount;
    }
}

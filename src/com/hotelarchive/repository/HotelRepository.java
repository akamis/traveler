package com.hotelarchive.repository;

import java.util.ArrayList;
import java.util.List;

import com.hotelarchive.message.HotelSearchResponse;
import com.hotelarchive.model.Hotel;
import com.hotelarchive.util.HibernateUtil;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class HotelRepository {

    public static final String SORT_AS_ASCENDING = "asc";
    public static final String SORT_AS_DESCENDING = "desc";
    public static final String DESTINATION_ID = "destinationId";
    public static final String HOTEL_ID = "hotelId";


    public HotelSearchResponse search(List<Integer> destinationList, int limit, int offset, String orderType, String orderBy) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Hotel.class);
        criteria.add(Restrictions.in(DESTINATION_ID, destinationList));


        ScrollableResults results = criteria.scroll();
        results.last();
        int totalCount = results.getRowNumber() + 1;
        results.close();

        if (orderType.equals(SORT_AS_ASCENDING)) {
            criteria.addOrder(Order.asc(orderBy));

        } else if (orderType.equals(SORT_AS_DESCENDING)) {
            criteria.addOrder(Order.desc(orderBy));

        }

        criteria.setFirstResult(offset);
        criteria.setMaxResults(limit);
        List<Hotel> hotels = criteria.list();
        return new HotelSearchResponse(hotels, totalCount);

    }


    public Hotel get(int hotelID) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Hotel.class);
        criteria.add(Restrictions.eq(HOTEL_ID, hotelID));
        Hotel hotel = (Hotel) criteria.uniqueResult();
        return hotel;
    }
}

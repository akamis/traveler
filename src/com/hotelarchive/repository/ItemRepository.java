package com.hotelarchive.repository;

import com.hotelarchive.model.Hotel;
import com.hotelarchive.model.Item;
import com.hotelarchive.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

public class ItemRepository {

    final public static String SELECT_ITEM_BY_ITEM_NAME_SQL = "from Item  where itemName like :keyWord";


    public List<Item> list(String itemName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(SELECT_ITEM_BY_ITEM_NAME_SQL);
        query.setParameter("keyWord", itemName + "%");
        query.setMaxResults(10);
        List<Item> items = query.list();

        return items;
    }
}

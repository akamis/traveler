package com.hotelarchive.repository;

import com.hotelarchive.model.Destination;
import com.hotelarchive.model.Hotel;
import com.hotelarchive.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import sun.security.krb5.internal.crypto.Des;

import java.util.List;

public class DestinationRepository {

    final public static String SELECT_DESTINATIONS_BY_DESTINATIONID_SQL = "SELECT d FROM Destination AS d LEFT OUTER JOIN FETCH d.childs WHERE d.destinationId=%d";

    public Destination get(int destinationId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(String.format(SELECT_DESTINATIONS_BY_DESTINATIONID_SQL, destinationId));
        Destination destination = (Destination) query.uniqueResult();
        return destination;
    }
}

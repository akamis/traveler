package com.hotelarchive.model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "destinations", schema = "traveler", catalog = "")
public class Destination {

    private int destinationId;
    private String destinationName;
    private Integer destinationTypeId;
    private Double longitude;
    private Double latitude;
    private int parentId;
    private List<Destination> childs;
//    private List<Hotel> hotels;

    @Id
    @Column(name = "destinationID")
    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    @Basic
    @Column(name = "destinationName")
    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @Basic
    @Column(name = "destinationTypeID")
    public Integer getDestinationTypeId() {
        return destinationTypeId;
    }

    public void setDestinationTypeId(Integer destinationTypeId) {
        this.destinationTypeId = destinationTypeId;
    }

    @Basic
    @Column(name = "longitude")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "latitude")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


    @Basic
    @Column(name = "parentID")
    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @ManyToMany
    @JoinTable(
            name = "destinations",
            joinColumns = @JoinColumn(name = "parentID"),
            inverseJoinColumns = @JoinColumn(name = "destinationID")
    )
    public List<Destination> getChilds() {
        return childs;
    }

    public void setChilds(List<Destination> childs) {
        this.childs = childs;
    }

//    @OneToMany
//    @JoinTable(
//            name="hotels",
//            joinColumns = @JoinColumn( name="destinationID"),
//            inverseJoinColumns = @JoinColumn( name="hotelID")
//    )
//    public List<Hotel> getHotels() {
//        return hotels;
//    }
//
//    public void setHotels(List<Hotel> hotels) {
//        this.hotels = hotels;
//    }
}

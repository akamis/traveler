package com.hotelarchive.model;


import javax.persistence.*;

/**
 * Created by nexaas on 29/05/2017.
 */
@Entity
@Table(name = "items", schema = "traveler", catalog = "")
public class Item {

    private int itemId;
    private int itemType;
    private String itemName;

    @Id
    @Column(name = "itemID")
    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    @Basic
    @Column(name = "itemType")
    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Basic
    @Column(name = "itemName")
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


}

package com.hotelarchive.business;

import com.hotelarchive.message.HotelDetailResponse;
import com.hotelarchive.message.HotelSearchRequest;
import com.hotelarchive.message.HotelSearchResponse;
import com.hotelarchive.repository.DestinationRepository;
import com.traveler.db.*;
import com.traveler.oldmodel.*;
import com.traveler.oldmodel.Destination;
import com.hotelarchive.repository.HotelRepository;
import sun.security.krb5.internal.crypto.Des;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class HotelBusiness {

    private List<Integer> destinationList = new ArrayList<Integer>();
    private List<com.traveler.oldmodel.Hotel> hotels;
    private com.traveler.oldmodel.Hotel hotel;
    PreparedStatement psAmentiy = null;
    PreparedStatement psAffiliate = null;
    PreparedStatement psDestination = null;
    PreparedStatement psReview = null;
    Connection con;
    ResultSet rs = null;
    ResultSet rsAmentiy = null;
    ResultSet rsAffiliate = null;
    ResultSet rsDestination = null;
    ResultSet rsReview = null;

    public List<com.traveler.oldmodel.Hotel> findPopularHotels(int destinationId, int count) {
        hotels = new ArrayList<com.traveler.oldmodel.Hotel>();
        return hotels;
    }

    public HotelDetailResponse getHotelDetail(int hotelID) {
        HotelRepository hotelRepository = new HotelRepository();
        com.hotelarchive.model.Hotel hotel = hotelRepository.get(hotelID);
        HotelDetailResponse hotelDetailResponse = new HotelDetailResponse(hotel);
        return hotelDetailResponse;
    }

    public void initDestinations(com.hotelarchive.model.Destination destination) {
        if (destination.getChilds() != null) {
            for (com.hotelarchive.model.Destination child : destination.getChilds()) {
                initDestinations(child);
            }
        }
        destinationList.add(destination.getDestinationId());

    }

    public HotelSearchResponse listHotels(HotelSearchRequest hotelSearchRequest) {
        DestinationRepository destinationRepository = new DestinationRepository();
        com.hotelarchive.model.Destination destination = destinationRepository.get(hotelSearchRequest.getDestinationId());
        initDestinations(destination);
        HotelRepository hotelRepository = new HotelRepository();
        HotelSearchResponse hotelsMessage = hotelRepository.search(destinationList, hotelSearchRequest.getLimit(), hotelSearchRequest.getPageNo() * hotelSearchRequest.getLimit(), hotelSearchRequest.getOrderType(), hotelSearchRequest.getOrderBy());

        return hotelsMessage;
    }

    public com.traveler.oldmodel.Hotel getHotel(ResultSet rs) throws SQLException {
        hotel = new com.traveler.oldmodel.Hotel();
        Destination destination = new Destination();
        hotel.setId(rs.getInt("HotelID"));
        hotel.setName(rs.getString("Name"));
        hotel.setStar(rs.getInt("Star"));

        if (rs.getFloat("Latitude") > (float) 0) {
            Coordinate coordinate = new Coordinate(rs.getFloat("Latitude"),
                    rs.getFloat("Longitude"));// ,rs.getInt("Zoom")
            hotel.setCoordinate(coordinate);

        }

        hotel.setDestinationID(rs.getInt("DestinationID"));
        hotel.setPhone(rs.getString("Phone"));
        hotel.setRoomCount(rs.getInt("RoomCount"));

        destination.setName(rs.getString("DESTINATIONNAME"));
        hotel.setDestination(destination);

        Map<String, Object> description = new HashMap<String, Object>();
        description.put("description", rs.getString("Description"));
        hotel.setDescription(description);

        Map<String, Object> address = new HashMap<String, Object>();
        address.put("postalCode", rs.getString("postalCode"));
        address.put("locality", rs.getString("locality"));
        address.put("country", rs.getString("country"));
        address.put("street", rs.getString("street"));
        hotel.setAddress(address);

        return hotel;

    }

    public void loadHotelAffiliates(com.traveler.oldmodel.Hotel hotel) {
        ArrayList<Affilate> affiliates = new ArrayList<Affilate>();
        Affilate affiliate;
        String hotelAffiliateQuery = "SELECT ha.COMPANY \n";
        hotelAffiliateQuery += ",ha.HOST  \n";
        hotelAffiliateQuery += ",ha.URI \n";

        hotelAffiliateQuery += "FROM HOTELAFFILIATES ha WHERE ha.HOTELID="
                + hotel.getId();
        try {
            psAffiliate = con.prepareStatement(hotelAffiliateQuery);

            rsAffiliate = psAffiliate.executeQuery();
            while (rsAffiliate.next()) {
                affiliate = new Affilate();
                affiliate.setCompany(rsAffiliate.getString("company"));
                affiliate.setHost(rsAffiliate.getString("host"));
                affiliate.setUri(rsAffiliate.getString("uri"));
                affiliate.setLogo("http://162.243.2.50/provider-logo/"
                        + rsAffiliate.getString("company").replace(".", "")
                        .replace(" ", "").toLowerCase() + ".gif");
                affiliates.add(affiliate);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
//			e.printStackTrace();
        }
        hotel.setAffiliates(affiliates);
    }

    public void loadHotelAmenities(com.traveler.oldmodel.Hotel hotel) {

        ArrayList<Amenity> amenities = new ArrayList<Amenity>();
        Amenity amenity;
        String hotelAmenityQuery = "SELECT HotelID \n";
        hotelAmenityQuery += ",la.AmenityID \n";
        hotelAmenityQuery += ",am.AmenityName \n";
        hotelAmenityQuery += "FROM HotelAmenities la join Amenities am on la.AmenityID=am.AmenityID WHERE HotelID="
                + hotel.getId();
        try {
            psAmentiy = con.prepareStatement(hotelAmenityQuery);

            rsAmentiy = psAmentiy.executeQuery();
            while (rsAmentiy.next()) {
                amenity = new Amenity(rsAmentiy.getInt("AmenityID"),
                        rsAmentiy.getString("AmenityName"));
                amenities.add(amenity);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        hotel.setAmenities(amenities);
    }

    public void loadHotelDestinationInfo(com.traveler.oldmodel.Hotel hotel) {
        String hotelDestinationQuery = "SELECT count(*) as hotelCount FROM HOTELS h WHERE h.DESTINATIONID="
                + hotel.getDestinationID();
        try {
            psDestination = con.prepareStatement(hotelDestinationQuery);

            rsDestination = psDestination.executeQuery();
            if (rsDestination.next()) {
                hotel.getDestination()
                        .setHotelCount(
                                Integer.parseInt(rsDestination
                                        .getString("hotelCount")));

            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void loadHotelImages(com.traveler.oldmodel.Hotel hotel, int imgCount) {

        PreparedStatement psImg = null;
        ResultSet rsImg = null;
        ArrayList<Map<String, Object>> images = new ArrayList<Map<String, Object>>();
        Map<String, Object> image;

        String hotelImageQuery = "SELECT \n";
        hotelImageQuery += "img.IMAGEID \n";
        hotelImageQuery += ",img.IMAGETRIPID \n";
        hotelImageQuery += ",img.IMAGETITLE \n";
        hotelImageQuery += ",img.IMAGETRIPFOLDER \n";
        hotelImageQuery += ",img.IMAGEFILENAME \n";
        hotelImageQuery += ",img.IMAGEFILEFORMAT \n";
        hotelImageQuery += "FROM ItemImages it join Images img on it.imageID=img.imageID WHERE it.itemId="
                + hotel.getId() + " and it.itemtypeid=2";
        try {
            psImg = con.prepareStatement(hotelImageQuery);
            rsImg = psImg.executeQuery();
            while (rsImg.next()) {
                image = new HashMap<String, Object>();
                image.put("id", rsImg.getInt("IMAGEID"));
                image.put("title", rsImg.getString("IMAGETITLE"));

                String imageHexId = org.apache.commons.lang.StringUtils
                        .leftPad(Integer.toHexString(rsImg
                                .getInt("IMAGETRIPID")), 8, '0');
                image.put(
                        "url",
                        "http://media-cdn.tripadvisor.com/media/" + "photo-s/"
                                + imageHexId.substring(0, 2) + "/"
                                + imageHexId.substring(2, 4) + "/"
                                + imageHexId.substring(4, 6) + "/"
                                + imageHexId.substring(6, 8) + "/"
                                + rsImg.getString("IMAGEFILENAME") + "."
                                + rsImg.getString("IMAGEFILEFORMAT"));

                // image.put("url", "slider/02.jpg");

                images.add(image);

            }
            if (images.size() == 0) {
                image = new HashMap<String, Object>();
                image.put("url", "img/no_thumb.gif");
                images.add(image);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        hotel.setImages(images);

    }

    public void loadHotelReviews(com.traveler.oldmodel.Hotel hotel) {

        List<Review> reviews = new ArrayList<Review>();
        Review review;
        String hotelReviewQuery = "SELECT * FROM HotelReviews hr WHERE hr.HotelID="
                + hotel.getId();
        try {
            psReview = con.prepareStatement(hotelReviewQuery);

            rsReview = psReview.executeQuery();
            while (rsReview.next()) {
                review = new Review();
                review.setRating(rsReview.getInt("RATING"));
                review.setTitle(rsReview.getString("TITLE"));
                review.setReview(rsReview.getString("REVIEW"));
                review.setServiceRating(rsReview.getInt("SERVICERATING"));
                review.setSleepQualityRating(rsReview
                        .getInt("SLEEPQUALITYRATING"));
                review.setLocationRating(rsReview.getInt("LOCATIONRATING"));
                review.setSwimmingPoolRating(rsReview
                        .getInt("SWIMMINGPOOLRATING"));
                review.setValueRating(rsReview.getInt("VALUERATINGRATING"));
                review.setCleanlinessRating(rsReview
                        .getInt("CLEANLINESSRATING"));
                review.setRoomsRating(rsReview.getInt("ROOMSRATING"));
                review.setFitnessFacilityRating(rsReview
                        .getInt("FITNESSFACILITYRATING"));
                reviews.add(review);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
//			e.printStackTrace();
        }
        hotel.setReviews(reviews);
    }
}

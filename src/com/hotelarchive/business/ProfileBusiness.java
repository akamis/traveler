package com.hotelarchive.business;

import com.traveler.db.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.brickred.socialauth.Profile;

public class ProfileBusiness {

    private Profile profile;
    PreparedStatement ps;
    Connection con;
    ResultSet rs;


    public Profile loginUser(String email, String password) {
        try {
            String userQuery = "select firstname,lastname,email,password,city from users where email = '"
                    + email + "' and password='" + password + "'";
            con = dbAccess.getConnection();
            ps = con.prepareStatement(userQuery);
            rs = ps.executeQuery();
            if (rs.next()) {

                profile = new Profile();
                profile.setFirstName(rs.getString("firstname"));
                profile.setLastName(rs.getString("lastname"));

                profile.setFullName(rs.getString("firstname")
                        + " " + rs.getString("lastname"));
                profile.setEmail(rs.getString("email"));

                profile.setLocation(rs.getString("city"));

                profile.setLastName(rs.getString("lastname"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return profile;
    }


    public boolean registerUser(String firstName, String lastName,
                                String email, String password, String city) {
        int i = 0;
        PreparedStatement ps = null;
        try {
            con = dbAccess.getConnection();
            if (con != null) {
                String sql = "INSERT INTO users(firstname, password, lastname, email,city) VALUES(?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, firstName);
                ps.setString(2, password);
                ps.setString(3, lastName);
                ps.setString(4, email);
                ps.setString(5, city);
                i = ps.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (i > 0) {
            return true;
        } else
            return false;
    }

}

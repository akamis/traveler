package com.hotelarchive.business;

import com.traveler.db.*;
import com.traveler.oldmodel.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class AmenityBusiness {

    private List<Amenity> amenities;
    private List<Amenity> filterStarList;
    PreparedStatement ps = null;
    PreparedStatement psAmentiy = null;
    Connection con;
    ResultSet rs = null;
    ResultSet rsAmentiy = null;

    public AmenityBusiness() {
    }


    public List<Amenity> getAmenities(int destinationId) {
        amenities = new ArrayList<Amenity>();
        String amenityQuery = "SELECT AMENITYID,AMENITYNAME, COUNT(*) as hotelcount FROM ( \n";
        amenityQuery += "SELECT h.HotelID  \n";
        amenityQuery += ",am.AMENITYID\n";
        amenityQuery += ",am.AMENITYNAME\n";
        amenityQuery += "FROM Hotels h \n";
        amenityQuery += "JOIN(\n";
        amenityQuery += "SELECT d1.DestinationID, \n";
        amenityQuery += "           d1.DestinationParentID, \n";
        amenityQuery += "           d2.DestinationID AS parent_id \n";
        amenityQuery += "      FROM Destinations d1 \n";
        amenityQuery += "LEFT JOIN Destinations d2 ON d2.DestinationID = d1.DestinationParentID \n";
        amenityQuery += "START WITH d1.DestinationID = " + destinationId
                + " \n";
        amenityQuery += "CONNECT BY PRIOR d1.DestinationID = d1.DestinationParentID \n";
        amenityQuery += ") d ON h.DestinationID = d.DestinationID \n";
        amenityQuery += "JOIN HOTELAMENITIES ha on ha.hotelid = h.hotelid  \n";
        amenityQuery += "JOIN AMENITIES am on ha.AMENITYID = am.AMENITYID ) \n";
        amenityQuery += " GROUP BY AMENITYID,AMENITYNAME \n";
        try {
            con = dbAccess.getConnection();
            ps = con.prepareStatement(amenityQuery);
            rs = ps.executeQuery();
            while (rs.next()) {
                amenities.add(new Amenity(rs.getInt("amenityid"), rs
                        .getString("amenityname"), rs.getInt("hotelcount")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return amenities;
    }


    public List<Amenity> getFilterStars(int destinationId) {

        filterStarList = new ArrayList<Amenity>();
        String starFilter = "SELECT star, COUNT(*) as hotelcount FROM ( SELECT star  \n";
        starFilter += "		FROM Hotels h  \n";
        starFilter += "		JOIN( \n";
        starFilter += "		SELECT d1.DestinationID,  \n";
        starFilter += "		           d1.DestinationParentID,  \n";
        starFilter += "		           d2.DestinationID AS parent_id  \n";
        starFilter += "		      FROM Destinations d1  \n";
        starFilter += "	LEFT JOIN Destinations d2 ON d2.DestinationID = d1.DestinationParentID  \n";
        starFilter += "		START WITH d1.DestinationID = " + destinationId
                + "  \n";
        starFilter += "		CONNECT BY PRIOR d1.DestinationID = d1.DestinationParentID  \n";
        starFilter += "		) d ON h.DestinationID = d.DestinationID  )  \n";
        starFilter += "		 where star >0  \n";
        starFilter += "		 GROUP BY  star  \n";
        try {
            con = dbAccess.getConnection();
            ps = con.prepareStatement(starFilter);
            rs = ps.executeQuery();
            while (rs.next()) {
                filterStarList.add(new Amenity(rs.getInt("star"), rs.getString("star"), rs
                        .getInt("hotelcount")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return filterStarList;
    }

}

package com.hotelarchive.business;

import com.traveler.db.*;
import com.traveler.oldmodel.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ReviewBusiness {

    PreparedStatement ps;
    Connection con;
    ResultSet rs;


    public Review findReview(Hotel hotel) {
        // TODO Auto-generated method stub
        return null;
    }


    public boolean addReview(Review review) {
        int i = 0;
        PreparedStatement ps = null;
        try {
            con = dbAccess.getConnection();
            if (con != null) {
                String sql = "INSERT INTO HOTELREVIEWS(USERID, HOTELID, RATING, TITLE, REVIEW, TRIPTYPE, TRIPDATE, SERVICERATING, SLEEPQUALITYRATING, LOCATIONRATING, SWIMMINGPOOLRATING, VALUERATINGRATING, CLEANLINESSRATING, ROOMSRATING, FITNESSFACILITYRATING, ROOMTIP) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                System.out.println(sql);
                ps = con.prepareStatement(sql);
                ps.setInt(1, 1);
                ps.setInt(2, review.getHotel().getId());
                ps.setInt(3, review.getRating());
                ps.setString(4, review.getTitle());
                ps.setString(5, review.getReview());
                ps.setString(6, review.getTripType());
                ps.setDate(7, review.getTripDate());
                ps.setInt(8, review.getServiceRating());
                ps.setInt(9, review.getSleepQualityRating());
                ps.setInt(10, review.getLocationRating());
                ps.setInt(11, review.getSwimmingPoolRating());
                ps.setInt(12, review.getValueRating());
                ps.setInt(13, review.getCleanlinessRating());
                ps.setInt(14, review.getRoomsRating());
                ps.setInt(15, review.getFitnessFacilityRating());
                ps.setString(16, review.getRoomTip());

                i = ps.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (i > 0) {
            return true;
        } else
            return false;
    }

}

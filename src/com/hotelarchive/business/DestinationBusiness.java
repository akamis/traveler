package com.hotelarchive.business;

import com.traveler.db.*;
import com.traveler.oldmodel.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class DestinationBusiness {

    private List<Destination> destinations;
    private Destination destination;
    private int destinationCount = 0;
    PreparedStatement ps = null;
    PreparedStatement psAmentiy = null;
    Connection con;
    ResultSet rs = null;
    ResultSet rsAmentiy = null;

    public DestinationBusiness() {

    }


    public List<Destination> findDestinations(int destinationId, int pageNo) {
        return destinations;
    }


    public Destination findDestination(int destinationId) {
        if (destinationId != 0) {

            try {

                String destinationQuery = "SELECT ds.DestinationID\n";
                destinationQuery += ",ds.DestinationTypeID \n";
                destinationQuery += ",ds.DestinationName\n";
                destinationQuery += ",co.Latitude \n";
                destinationQuery += ",co.Longitude \n";
                destinationQuery += ",co.Zoom \n";
                destinationQuery += ",de.Description\n";
                destinationQuery += ",de.DescriptionTitle\n";
                destinationQuery += "FROM Destinations ds  \n";
                destinationQuery += "LEFT OUTER JOIN Coordinates co on ds.DestinationID = co.ItemID and co.ItemTypeID=1\n";
                destinationQuery += "LEFT OUTER JOIN Descriptions de on ds.DestinationID = de.ItemID and de.ItemTypeID=1\n";
                destinationQuery += "WHERE ds.DestinationID=" + destinationId;

                con = dbAccess.getConnection();
                ps = con.prepareStatement(destinationQuery);
                rs = ps.executeQuery();
                if (rs.next()) {
                    destination = new Destination();
                    destination.setId(rs.getInt("DestinationID"));
                    destination.setTypeId(rs.getInt("DestinationTypeID"));
                    destination.setName(rs.getString("DestinationName"));

                    if (rs.getFloat("Latitude") > (float) 0) {
                        Coordinate coordinate = new Coordinate(
                                rs.getFloat("Latitude"),
                                rs.getFloat("Longitude"));// ,rs.getInt("Zoom")
                        destination.setCoordinate(coordinate);

                    }

                    Map<String, Object> description = new HashMap<String, Object>();
                    description.put("description", rs.getString("Description"));
                    description.put("title", rs.getString("DescriptionTitle"));
                    destination.setDescription(description);
                    String destinationImageQuery = "SELECT img.IMAGEID \n";
                    destinationImageQuery += ",img.IMAGETRIPID \n";
                    destinationImageQuery += ",img.IMAGETITLE \n";
                    destinationImageQuery += ",img.IMAGETRIPFOLDER \n";
                    destinationImageQuery += ",img.IMAGEFILENAME \n";
                    destinationImageQuery += ",img.IMAGEFILEFORMAT \n";
                    destinationImageQuery += "FROM ItemImages it JOIN Images img on img.Imageid = it.Imageid \n";
                    destinationImageQuery += "WHERE it.ItemTypeID=1 and it.ItemID="
                            + destinationId + " \n";

                    ps = con.prepareStatement(destinationImageQuery);
                    ResultSet rsImg = ps.executeQuery();
                    ArrayList<Map<String, Object>> images = new ArrayList<Map<String, Object>>();
                    Map<String, Object> image;
                    while (rsImg.next()) {
                        image = new HashMap<String, Object>();
                        image.put("id", rsImg.getInt("IMAGEID"));
                        image.put("title", rsImg.getString("IMAGETITLE"));
                        String imageHexId = org.apache.commons.lang.StringUtils
                                .leftPad(Integer.toHexString(rsImg
                                        .getInt("IMAGETRIPID")), 8, '0');

                        image.put("url",
                                "http://media-cdn.tripadvisor.com/media/" +
                                        rsImg.getString("IMAGETRIPFOLDER") + "/" +
                                        imageHexId.substring(0, 2) + "/" +
                                        imageHexId.substring(2, 4) + "/" +
                                        imageHexId.substring(4, 6) + "/" +
                                        imageHexId.substring(6, 8) + "/" +
                                        rsImg.getString("IMAGEFILENAME") + "." +
                                        rsImg.getString("IMAGEFILEFORMAT"));

                        //image.put("url", "slider/02.jpg");
                        images.add(image);
                    }
                    if (images.size() == 0) {
                        image = new HashMap<String, Object>();
                        image.put("url", "img/1280x852.jpg");
                        images.add(image);
                    }
                    destination.setImages(images);

                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    con.close();
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return (destination);
        } else {
            return (null);
        }
    }

    public void addDestination(Destination destination) {
        destinations.add(destination);
    }

    public int getDestinationCount() {
        return destinationCount;
    }

    public ArrayList<Map<String, Object>> getDestinationMap(int destinationId) {
        ArrayList<Map<String, Object>> destinationList = null;
        String destinationListQuery = "SELECT d1.DestinationID, \n";
        destinationListQuery += "d1.DestinationName\n";
        destinationListQuery += "FROM Destinations d1\n";
        destinationListQuery += "LEFT JOIN Destinations d2 ON d2.DestinationID = d1.DestinationParentID\n";
        destinationListQuery += "START WITH d1.DestinationID = "
                + destinationId + "\n";
        destinationListQuery += "CONNECT BY PRIOR d1.DestinationParentID = d1.DestinationID\n";
        destinationListQuery += "ORDER BY d1.DestinationID asc\n";
        System.out.println(destinationListQuery);
        con = dbAccess.getConnection();
        try {
            ps = con.prepareStatement(destinationListQuery);
            rs = ps.executeQuery();
            destinationList = new ArrayList<Map<String, Object>>();
            Map<String, Object> destinationMap;
            while (rs.next()) {
                destinationMap = new HashMap<String, Object>();
                destinationMap.put("destinationId", rs.getInt("destinationID"));
                destinationMap.put("destinationName",
                        rs.getString("destinationName"));
                destinationList.add(destinationMap);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
                ps.close();
            } catch (Exception e) {
//				e.printStackTrace();
            }
        }
        return destinationList;

    }
}

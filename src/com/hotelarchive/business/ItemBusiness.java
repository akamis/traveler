package com.hotelarchive.business;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import com.hotelarchive.message.ItemSearchResponse;
import com.hotelarchive.repository.ItemRepository;
import com.traveler.oldmodel.Item;

public class ItemBusiness {

    private List<Item> items;
    private Item item;


    public ItemSearchResponse listItems(String itemName) {
        ItemRepository itemRepository = new ItemRepository();
        List<com.hotelarchive.model.Item> items = itemRepository.list(itemName);
        ItemSearchResponse hotelDetailResponse = new ItemSearchResponse(items);
        return hotelDetailResponse;
    }


}
